// ==UserScript==
// @name         dw simple
// @version      0.3.7
// @description  clean up lynx and add tagging tools
// @author       pynt
// @match        *://*.dreamwidth.org/*
// @downloadURL  https://gitlab.com/pynt/spek/-/raw/master/dws.js
// @updateURL    https://gitlab.com/pynt/spek/-/raw/master/dws.js
// @grant        GM_addElement
// @grant        GM_setValue
// @grant        GM_getValue
// @grant        GM_listValues
// @grant        GM_deleteValue
// ==/UserScript==
/* eslint-disable no-undef */
(function() {
  'use strict';
  const loadFont = false;

  if (loadFont && Array.from(document.querySelectorAll('link[rel=stylesheet]')).filter((l) => l.href.includes('lynx')).length) {
    GM_addElement('link', {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,400;0,700;1,400;1,700&display=swap'
    });

    GM_addElement('style', {
      type: 'text/css',
      textContent: 'body { font-family:"Lato", sans-serif; } a, a:visited { text-decoration:none; color:#00e; } a:hover { text-decoration:underline; }',
    });
  }

  const qrdiv = document.getElementById('qrdiv') || document.getElementsByName('qrdiv')[0];
  if (!qrdiv) {
    return
  }

  // metadata
  const user = document.querySelector('input#cookieuser').getAttribute('value');
  const journal = document.querySelector('input#journal')?.getAttribute('value');
  const page = document.querySelector('input#itemid').getAttribute('value');
  const comment = document.querySelector('input#dtid').getAttribute('value');
  const type = user === 'counterstep';

  const subject = document.getElementById('subject') || document.getElementsByName('subject')[0];
  const body = document.getElementById('body') || document.getElementById('commenttext') || document.getElementsByName('body')[0];
  const icon = document.getElementById('prop_picture_keyword');
  const storageKeyArray = [ user, journal, page, comment ];
  const storageKey = () => {
    return storageKeyArray.join(':');
  }

  const placeholder = 'TEXT';
  let transitioning = false;
  let $historyList;

  const observer = new MutationObserver(textChangeCallback);
  if ($('input#dtid').get().length) {
    observer.observe($('input#dtid').get()[0], { attributes: true, childList: true, subtree: true })
  }

  let lastComment = GM_getValue(storageKey()) || {};
  if(body) {
    if (!body.value && (lastComment.subject || lastComment.icon || lastComment.text)) {
      subject.value = lastComment.subject || subject.value;
      icon.value = lastComment.icon || icon.value;
      body.value = lastComment.text || body.value;
    }

    body.oninput = ((lastComment, t, e) => {
      const currentKey = storageKey();
      updateStorageKey()
      const activeKey = storageKey();
      console.log(currentKey !== activeKey)
      if (currentKey !== activeKey) {
        return
      } else if (!t && lastComment.text === undefined || lastComment.text !== e.target.value) {
        if (e.target.value !== '') {
          lastComment.text = e.target.value
        } else {
          delete lastComment.text
        }
        GM_setValue(storageKey(), lastComment)
      }
    }).bind(this, lastComment, transitioning)

    subject.oninput = ((lastComment, t, e) => {
      const currentKey = storageKey();
      updateStorageKey()
      const activeKey = storageKey();
      console.log(currentKey !== activeKey)
      if (currentKey !== activeKey) {
        return
      } else if (!t && lastComment.subject === undefined || lastComment.subject !== e.target.value) {
        if (e.target.value !== '') {
          lastComment.subject = e.target.value
        } else {
          delete lastComment.subject
        }
        GM_setValue(storageKey(), lastComment)
      }
    }).bind(this, lastComment, transitioning)

    icon.onchange = ((lastComment, t, e) => {
      const currentKey = storageKey();
      updateStorageKey()
      const activeKey = storageKey();
      console.log(currentKey !== activeKey)
      if (currentKey !== activeKey) {
        return
      } else if (!t && lastComment.icon === undefined || lastComment.icon !== e.target.value) {
        if (e.target.value !== '') {
          lastComment.icon = e.target.value
        } else {
          delete lastComment.icon
        }
        GM_setValue(storageKey(), lastComment)
      }
    }).bind(this, lastComment, transitioning)
  }

  if (!type) {
    GM_addElement('script', {
      type: 'text/javascript',
      src: 'https://www.dreamwidth.org/js/??jquery.replyforms.js,jquery.poll.js,journals/jquery.tag-nav.js,jquery.mediaplaceholder.js,jquery.imageshrink.js,components/jquery.icon-select.js,jquery.quickreply.js,jquery.threadexpander.js,foundation/foundation/foundation.reveal.js,components/jquery.icon-browser.js,jquery/jquery.ui.button.js,jquery/jquery.ui.dialog.js,jquery.commentmanage.js,jquery.esn.js,skins/jquery.focus-on-reveal.js?v=1715910518'
    })
    GM_addElement('link', {
        rel: 'stylesheet',
        href: 'https://www.dreamwidth.org/stc/??css/components/quick-reply.css,css/components/icon-select.css,css/components/imageshrink.css,css/components/icon-browser.css,jquery/jquery.ui.button.css,jquery/jquery.ui.dialog.css,jquery.commentmanage.css,jquery/jquery.ui.theme.smoothness.css,canary.css,css/components/foundation-icons.css?v=1715976107'
    });
  }

  createButton('Δ', undefined, undefined, undefined, undefined, populateHistoryList)
  createButton('⬡', '<small>( ', ' )</small>');
  createButton('↬', '<a href=>', '</a>');
  createButton('«', type ? '<code>' : `<span style="font-family:monospace;">`, type ? '</code>' : '</span>');
  createButton('i', '<i>', '</i>', 'i');
  createButton('b', '<b>', '</b>', 'b');
  createButton('↯', '<small style="display:block;text-align:right;">( <a href=', '>from</a> )</small>', undefined, 'URL');

  function createButton(title, start, end, style = undefined, customPlaceholder = undefined, handler = undefined) {
    const btn = document.createElement('button');
    btn.appendChild(document.createTextNode(title));
    btn.style.display = 'inline-flex';
    btn.style.alignItems = 'center';
    btn.style.justifyContent = 'center';
    btn.style.marginLeft = '2px';
    if (qrdiv.style.display === 'none') {
      document.getElementsByTagName('body')[0].appendChild(qrdiv)
      qrdiv.style.visibility = 'hidden';
      qrdiv.style.display = 'block';
      btn.style.height = `${subject.offsetHeight}px`;
      btn.style.width = `${subject.offsetHeight}px`;
      btn.style.fontSize = `${subject.offsetHeight - 6 > 12 ? subject.offsetHeight - 6 : 12}px`;
      qrdiv.style.display = 'none';
      qrdiv.style.visibility = 'visible';
    } else {
      btn.style.height = `${subject.offsetHeight}px`;
      btn.style.width = `${subject.offsetHeight}px`;
      btn.style.fontSize = `${subject.offsetHeight - 6 > 12 ? subject.offsetHeight - 6 : 12}px`;
    }
    btn.onclick = (e) => {
      e.preventDefault();

      if (handler) {
        handler();
      } else {
        const head = body.value.slice(0, body.selectionStart);
        const selection = body.value.slice(body.selectionStart, body.selectionEnd) || customPlaceholder || placeholder;
        const tail = body.value.slice(body.selectionEnd);

        if (document.execCommand !== undefined) { // process with history if supported
          document.execCommand('insertText', false, start + selection + end)
        } else { // otherwise use old method
          body.value = head + start + selection + end + tail;
        }

        body.focus();
        body.setSelectionRange((head + start).length, (head + start + selection).length);
      }
    };

    switch (style) {
      case 'i':
        btn.style.fontStyle = 'italic';
        break;
      case 'b':
        btn.style.fontWeight = 'bold';
        break;
      default:
    }

    subject.parentNode.appendChild(btn);
  }

  function populateHistoryList() {
    if (!$('#history-manager').length) {
      $('body').append(`
        <div class="reveal-modal icon-browser no-meta" id="history-manager" data-reveal>
        <div>
          <select id="history-list" multiple>
          </select>
          <button type="button" id="history-delete">Clear History</button>
        </div>
        <div id="history-text"></div>
        <button type="button" class="fi-icon--with-fallback close-reveal-modal">
            <span class="fi-icon" aria-hidden="true">✖︎</span>
            <span class="fi-icon--fallback">✖︎</span>
        </button>
      </div>`)

      $historyList = $('#history-list')
      $historyList.on('change', populateHistoryText)
      $historyList.on('keyup', (e) => {
        if (e.key === 'Delete') {
          $('#history-list > option:selected').each((i) => {
            GM_deleteValue($('#history-list > option:selected')[i].innerText);
          })
          $("#history-list > option:selected").remove()
          populateHistoryText()
        }
      })

      $('#history-delete').css({
        border: '1px #ddd solid',
        borderRadius: '3px',
        padding: '3px 5px',
        background: '#eee',
      })
      $('#history-delete').on("click", () => {
        clearHistory()
      })
    }

    const db = GM_listValues()
    $historyList.empty()
    for (const key of db) {
      $historyList.append('<option>'+key+'</option>')
    }
    $('#history-manager').foundation('reveal', 'open')
  }

  function populateHistoryText() {
    $('#history-text').empty()
    $('#history-list > option:selected').each((i) => {
      const key = $('#history-list > option:selected')[i].innerText;
      $('#history-text').append('<p>'+key+' '+GM_getValue(key).icon+' '+GM_getValue(key).subject+' '+'</p>')
      $('#history-text').append('<pre style="white-space:pre-wrap;"></pre>')
      $('#history-text pre').last().text(GM_getValue(key).text)
    })
  }

  function clearHistory() {
    const db = GM_listValues()
    for (const key of db) {
      GM_deleteValue(key)
      $historyList.empty()
      $('#history-text').empty()
    }
  }

  function textChangeCallback(mutationList/*, observer*/) {
    for (const mutation of mutationList) {
      transitioning = true;
      console.log(mutation)
      updateStorageKey().then(() => {
        lastComment = GM_getValue(storageKey());
        if (lastComment?.subject || lastComment?.icon || lastComment?.text) {
          console.log(lastComment, subject.value, icon.value, body.value)
          if (lastComment.subject !== '' && lastComment.subject !== undefined) {
            subject.value = lastComment.subject;
          }
          if (lastComment.icon !== '' && lastComment.icon !== undefined) {
            icon.value = lastComment.icon;
          }
          if (lastComment.text !== '' && lastComment.text !== undefined) {
            body.value = lastComment.text;
          }
          $(".block-icon").find("img").attr("src", $("#prop_picture_keyword").find("option:selected").data("url")).removeAttr("width").removeAttr("height").removeAttr("alt");
          if ($("#prop_picture_keyword").find("option:selected").attr("value") === "") {
            $(".block-icon").addClass("default")
          } else {
            $(".block-icon").removeClass("default")
          }
        }
        transitioning = false;
      })
    }
  }

  function updateStorageKey() {
    return Promise.resolve(storageKeyArray[3] = document.querySelector('input#dtid').getAttribute('value'));
  }

  // TODO: icon selector 0.4
})();
